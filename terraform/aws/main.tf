
resource "aws_key_pair" "deployer" {
  key_name   = "deployer-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDPP11xAjzBX3leRfGTkQYZRNi3A9oofGgSDtb9W1StRSy6bBCmpcqTE2LD8SVkU6b8Jr5MdDgTX89x40yl+W1NRIORkRjRk97ctRgxkzBhU/jMC2L/63xQi5OdMRmXaME4LbfLgA4IWxbPSH4kJTOeAsdWQu+nz0hl6sP7QlLTKQs4rm5rC/B1YZJIcv7pmWfcy1h8KozqzMABFFk/HPd+1jNK7cPOmL7QDijCUGmqSJEot8Az0YVVpIMwgIpdzi0qVBUAi1y6KKYV9vRsKBokMO8zIbSb0qVvq3ifrw/AJLSTme8j54WnFJiKGG7z+gPPuRO8Qqy0uNe1IvMDsHC5"
}


resource "aws_default_vpc" "default" {
  tags = {
    Name = "Default VPC"
  }
}

resource "aws_vpc" "mainvpc" {
  cidr_block = "10.1.0.0/16"
}

resource "aws_default_security_group" "default" {
  vpc_id = "${aws_vpc.mainvpc.id}"

  ingress {
    protocol  = -1
    self      = true
    from_port = 0
    to_port   = 0
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}




provider "aws" {
  region = "us-west-2"
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-trusty-14.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_instance" "web" {
  ami           = "${data.aws_ami.ubuntu.id}"
  instance_type = "t2.micro"

  tags = {
    Name = "HelloWorld"
  }
}

